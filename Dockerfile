FROM cloudron/base:4.2.0@sha256:46da2fffb36353ef714f97ae8e962bd2c212ca091108d768ba473078319a47f4

RUN mkdir -p /app/code /opt/docker

# static archive - https://download.docker.com/linux/static/stable/x86_64/
# renovate: datasource=github-releases depName=moby/moby versioning=semver extractVersion=^v(?<version>.+)$
ARG DOCKER_VERSION=28.0.1
RUN curl https://download.docker.com/linux/static/stable/x86_64/docker-${DOCKER_VERSION}.tgz | tar zxvf - -C /opt/docker --strip-components=1
ENV PATH=/opt/docker:$PATH

WORKDIR /app/code/frontend
COPY frontend/ /app/code/frontend/
RUN npm install && npm run build
RUN chown -R cloudron:cloudron /app/code/frontend/dist

WORKDIR /app/code
COPY src/ /app/code/src/
COPY app.js worker.js start.sh package.json package-lock.json /app/code/
RUN npm install --omit=dev

CMD [ "/app/code/start.sh" ]
