#!/bin/bash

set -eu -o pipefail

export DOCKER_HOST="${CLOUDRON_DOCKER_HOST}"

if [[ ! -f /app/data/docker.json ]]; then
    cat > /app/data/docker.json <<EOF
{
  "registry.docker.com": {
    "username": "",
    "password": ""
  }
}
EOF
fi

chown -R cloudron:cloudron /app/data

echo "=> Running build service"
exec /usr/local/bin/gosu cloudron:cloudron /app/code/app.js
