#!/usr/bin/env node

'use strict';

const janitor = require('./src/janitor.js'),
    server = require('./src/server.js'),
    taskq = require('./src/taskq.js');

const PORT = process.env.PORT || 3000;
const APP_ORIGIN = process.env.CLOUDRON_APP_ORIGIN || `http://localhost:${PORT}`;

taskq.initialize();
janitor.initialize();

server.start(PORT, (error) => {
    if (error) {
        console.error('Failed to start server:', error);
        process.exit(1);
    }

    console.log(`Build Service started at ${APP_ORIGIN}`);
});
