import { createApp } from 'vue';

import 'boxicons/css/boxicons.min.css'

import App from './App.vue';

import './main.css';

const app = createApp(App);

app.mount('#app')
