#!/usr/bin/env node

'use strict';

var assert = require('assert'),
    fs = require('fs'),
    safe = require('safetydance'),
    Docker = require('dockerode'),
    url = require('url'),
    util = require('util'),
    split = require('split');

if (process.argv.length !== 5) {
    console.log(`Got ${process.argv.length} args. Expecting 5`);
    console.log('Usage: ./worker.js <buildJson> <authJson> <logFilePath>');
    process.exit(1);
}

const build = JSON.parse(process.argv[2]);
const buildId = build.id;
const sourceArchiveFilePath = build.sourceArchiveFilePath;
const noPush = build.noPush;
const dockerImageRepo = build.dockerImageRepo; // contains registry already
const dockerImageTag = build.dockerImageTag;
const noCache = build.noCache;
const dockerfile = build.dockerfileName;
const buildStatus = build.status;
const dockerAuth = JSON.parse(process.argv[3]);
const localBuildLogFilePath = process.argv[4];
const buildArgs = build.buildArgs || {};

console.log();
console.log('===========================================');
console.log('     Application Build Service Worker      ');
console.log('===========================================');
console.log();
console.log(`Docker Username:   ${dockerAuth.username}`);
console.log(`Build logfile:     ${localBuildLogFilePath}`);
console.log(`Build Id:          ${build.id}`);
console.log(`Image Repo:        ${build.dockerImageRepo}`);
console.log(`Image Tag:         ${build.dockerImageTag}`);
console.log(`Push:              ${!build.noPush}`);
console.log(`Source Archive:    ${build.sourceArchiveFilePath}`);
console.log(`Use cache:         ${!build.noCache}`);
console.log(`Dockerfile Name:   ${build.dockerfileName}`);
console.log();
console.log('===========================================');
console.log();

const dockerOptions = {};
if (process.env.DOCKER_HOST) {
    const tmp = url.parse(process.env.DOCKER_HOST);
    dockerOptions.host = tmp.hostname;
    dockerOptions.port = tmp.port;
} else {
    dockerOptions.socketPath = '/var/run/docker.sock';
}

const docker = new Docker(dockerOptions);

const dockerImageName = `${dockerImageRepo}:${dockerImageTag}`;
const SYNTAX_ERROR_MARKER = ' - Syntax error - ';

var logFileStream = fs.createWriteStream(localBuildLogFilePath, { encoding: 'utf-8' });

function log(obj) {
    var message;

    if (typeof obj === 'string') {
        message = obj;
        obj = { message: obj };
    } else if (util.isError(obj)) { // special because JSON.stringify does not include message!
        message = obj.message;
        obj = { error: obj.message };
    } else {
        message = JSON.stringify(obj);
    }

    console.log('%s %s', buildId, message);
}

function logToFile(obj) {
    logFileStream.write(JSON.stringify(obj) + '\n');
}

function exit(error, code) {
    if (error) log(error, code);

    logFileStream.close();
    safe.fs.unlinkSync(sourceArchiveFilePath);

    process.exit(code ? code : (error ? 10 : 0));
}

function buildDockerImage(sourceArchiveStream, callback) {
    assert.strictEqual(typeof sourceArchiveStream, 'object');
    assert.strictEqual(typeof callback, 'function');

    var hasBuildError = false;

    console.log(`${buildId} docker build -t ${dockerImageName} (${dockerfile})`);

    docker.buildImage(sourceArchiveStream, { t: dockerImageName, nocache: noCache, dockerfile: dockerfile, buildargs: buildArgs, forcerm: true }, function (error, output) {
        if (error) {
            let message = '';
            // try to extract a Dockerfile syntax error
            if (error.message.indexOf(SYNTAX_ERROR_MARKER) !== -1) {
                message = 'Dockerfile syntax error: ' + error.message.slice(error.message.indexOf(SYNTAX_ERROR_MARKER) + SYNTAX_ERROR_MARKER.length);
            } else if(error.message.indexOf('Dockerfile parse error') !== -1) {
                // there is a hidden error object in the string
                const tmp = safe.JSON.parse(error.message.slice(error.message.indexOf('{')));
                message = tmp.message ? tmp.message : error.message;
            } else {
                message = error.message;
            }

            logToFile({ errorDetail: { message: message }});

            return callback(error);
        }

        output.setEncoding('utf8');
        var transformStream = output.pipe(split());

        transformStream.on('data', function (data) {
            if (!data) return; // it's not really LD-JSON :-(

            var obj = safe.JSON.parse(data);
            if (!obj) return log('Error parsing data [' + data + '] ' + data.length);

            if (obj.error || obj.errorDetail) hasBuildError = true; // sometimes we get errorDetail without error

            logToFile(obj);
        });

        transformStream.on('error', function (error) {
            log('Build stream error:' + error.message);

            callback(error);
        });

        transformStream.on('end', function () {
            log('Build stream finished');

            callback(null, hasBuildError);
        });
    });
}

function pushDockerImage(callback) {
    assert.strictEqual(typeof callback, 'function');

    var pushError = false;

    var image = docker.getImage(dockerImageRepo);
    image.push({ tag: dockerImageTag }, function (error, output) {
        if (error) {
            logToFile({ errorDetail: { message: error.message }});
            return callback(error);
        }

        output.setEncoding('utf8');
        var transformStream = output.pipe(split());

        transformStream.on('data', function (data) {
            if (!data) return; // it's not really LD-JSON :-(

            var obj = safe.JSON.parse(data);
            if (!obj) return log('Error parsing data [' + data + '] ' + data.length);

            if (obj.error || obj.errorDetail) pushError = obj.error || obj.errorDetail; // sometimes we get errorDetail

            logToFile(obj);
        });
        transformStream.on('error', function (error) {
            log('Push stream error:' + error.message);

            callback(error);
        });

        transformStream.on('end', function () {
            log('Push stream finished');

            callback(null, pushError);
        });
    }, dockerAuth);
}

function main() {
    let buildFunc;
    if (buildStatus === 'pending_push') {
        log('Skipping build since only push requested');
        buildFunc = (next) => next(null, false /* hasBuildError */);
    } else {
        const sourceArchiveStream = fs.createReadStream(sourceArchiveFilePath);

        buildFunc = buildDockerImage.bind(null, sourceArchiveStream);
    }

    buildFunc(function (error, hasBuildError) {
        if (error || hasBuildError) exit(error ? error.message : 'Build error', 2);

        if (noPush) return exit();

        log('Pushing...');

        var triesLeft = 20;

        (function push() {
            pushDockerImage(function (error, pushError) {
                if (error) {
                    log('Internal push error', error.message);
                    exit('Internal push error', 3);
                } else if (pushError && pushError.indexOf('already in progress') !== -1 && triesLeft >= 0) {
                    log('Waiting for other push to finish...');
                    --triesLeft;
                    setTimeout(push, 5000);
                    return;
                } else if (pushError) {
                    log(pushError);
                    exit('Push failed', 3);
                }

                exit();
            });
        })();
    });
}

// main flow
log('Building...');

main();
