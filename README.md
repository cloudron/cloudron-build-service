Cloudron Build Service
======================

Docker in docker
---------------

It's probably a good idea to use docker in docker. We can use it to then limit the CPU and Memory.

Advantages:

* No pollution of host images and host docker configuration

Gotchas:

* Does not work without --privileged flag thus removing containerization benefits.
* It uses the vfs storage driver. This driver will create a deep copy for each layer. Insane storage.

Conclusion so far: given we have no way to limit app storage sizes in platform level atm, this has limited benefit.

See also https://dev.to/code42cate/dont-build-docker-apps-without-this-flag-48kg

