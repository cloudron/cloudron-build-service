'use strict';

exports = module.exports = {
    initialize
};

const builds = require('./builds.js'),
    safe = require('safetydance');

function initialize() {
    if (process.env.CLOUDRON) {
        setInterval(async function () {
            console.log('janitor: cleaning up dangling images and old builds');

            safe.child_process.execSync(`${__dirname}/remove-dangling.sh`);

            const [error] = await safe(builds.cleanupBuilds());
            if (error) console.error('Failed to cleanup builds.', error);
            else console.log('Done cleaning builds', error);
        }, 6 * 60 * 60 * 1000); // 6 hours
    }
}
