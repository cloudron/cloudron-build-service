'use strict';

exports = module.exports = {
    add,
    start,
    stop,
    getAll,
    get,
    startLogStream,
    cleanupBuilds,
    push
};

const gTasks = {};

const assert = require('assert'),
    builddb = require('./builddb.js'),
    child_process = require('child_process'),
    fs = require('fs'),
    path = require('path'),
    safe = require('safetydance'),
    Tail = require('tail').Tail;

async function add(build) {
    assert.strictEqual(typeof build, 'object');

    assert.strictEqual(typeof build.noPush, 'boolean');
    assert.strictEqual(typeof build.dockerImageRepo, 'string');
    assert.strictEqual(typeof build.dockerImageTag, 'string');
    assert.strictEqual(typeof build.sourceArchiveFilePath, 'string');
    assert.strictEqual(typeof build.noCache, 'boolean');
    assert.strictEqual(typeof build.dockerfileName, 'string');
    assert.strictEqual(typeof build.buildArgs, 'object');

    console.log(`Scheduling new build. ${build.dockerImageRepo}:${build.dockerImageTag} from ${build.sourceArchiveFilePath} noCache:${build.noCache} dockerfileName:${build.dockerfileName} noPush:${build.noPush}`);

    build.id = 'build-' + (Date.now());
    await builddb.add(build);

    console.log(`Scheduled ${build.id}`);
    return build.id;
}

async function push(buildId, dockerImageRepo, dockerImageTag) {
    assert.strictEqual(typeof buildId, 'string');
    assert.strictEqual(typeof dockerImageRepo, 'string');
    assert.strictEqual(typeof dockerImageTag, 'string');

    const b = await builddb.get(buildId);
    if (!b) throw new Error('No such build');
    if (b.status !== 'success') throw new Error('Only successful builds can be pushed');

    // rename the existing docker image
    console.log(`Prepare ${buildId} for push. Renaming existing docker image ${b.dockerImageRepo}:${b.dockerImageTag} to ${dockerImageRepo}:${dockerImageTag}`);
    safe.child_process.execSync(`docker tag ${b.dockerImageRepo}:${b.dockerImageTag} ${dockerImageRepo}:${dockerImageTag}`);
    if (safe.error) throw new Error(`Failed to rename docker image: ${safe.error.message}`);

    console.log(`Scheduling new push ${buildId}`);
    const buildLogFilePath = path.join('/tmp', `${buildId}.log`);
    await fs.promises.truncate(buildLogFilePath, 0); // otherwise you see old build logs which is confusing

    await builddb.update(buildId, {
        status: 'pending_push',
        dockerImageRepo,
        dockerImageTag
    });
}

async function getAll() {
    const result = await builddb.getAll();

    const builds = Object.keys(result).map(function (id) { return result[id]; });

    return builds;
}

async function get(buildId) {
    assert.strictEqual(typeof buildId, 'string');

    return await builddb.get(buildId);
}

async function start(build) {
    assert.strictEqual(typeof build, 'object');

    const dockerImageName = `${build.dockerImageRepo}:${build.dockerImageTag}`;
    const buildLogFilePath = path.join('/tmp', `${build.id}.log`);

    console.log(`Starting building ${build.id} as ${dockerImageName} from ${build.sourceArchiveFilePath} noCache:${build.noCache} dockerfileName:${build.dockerfileName} noPush:${build.noPush}`);

    const dockerConfigFilePath = process.env.CLOUDRON ? '/app/data/docker.json' : 'docker.json';
    const authInfo = safe.JSON.parse(safe.fs.readFileSync(dockerConfigFilePath, 'utf8')) || {};
    if (safe.error) console.log(`WARNING: Error loading credentials from ${dockerConfigFilePath}`, safe.error);

    const parts = build.dockerImageRepo.split('/');
    // https://github.com/docker/distribution/blob/release/2.7/reference/normalize.go#L62
    let registry = 'docker.io';
    if (parts.length > 1 && (parts[0].match(/[.:]/) !== null)) registry = parts[0];
    let dockerAuth = null;

    if (registry === 'docker.io' || registry === 'index.docker.io' || registry === 'registry.docker.com') {
        dockerAuth = authInfo['docker.io'] || authInfo['index.docker.io'] || authInfo['registry.docker.com'] || {};
    } else {
        dockerAuth = authInfo[registry] || {};
    }

    const [error] = await safe(builddb.updateStatus(build.id, 'running'));
    if (error) return console.error(error);

    // spawn tasks now
    gTasks[build.id] = child_process.fork(path.join(__dirname, '../worker.js'), [
        JSON.stringify(build),
        JSON.stringify(dockerAuth),
        buildLogFilePath
    ], { stdio: 'inherit' });
    gTasks[build.id].on('error', async function (error) {
        console.log('worker process died: %s', error);

        delete gTasks[build.id];

        const [updateError] = await safe(builddb.updateStatus(build.id, 'failed'));
        if (updateError) return console.error(updateError);
    });

    gTasks[build.id].on('close', async function (code) {
        delete gTasks[build.id];

        // Exit codes are:
        // 0 - Success (the process succeeded, but the build might or might not have succeed)
        // 1 - Usage Error
        // 2 - Build Failure
        // 3 - Push Failure

        if (code === 0) {
            console.log(`Build ${build.id} succeeded.`);
            const [updateError] = await safe(builddb.updateStatus(build.id, 'success'));
            if (updateError) return console.error(updateError);
        } else if (code === 1) {
            console.error(`Build ${build.id} failed. Wrong worker usage.`);
            const [updateError] = await safe(builddb.updateStatus(build.id, 'failed'));
            if (updateError) return console.error(updateError);
        } else if (code === 2) {
            console.error(`Build ${build.id} failed during image building.`);
            const [updateError] = await safe(builddb.updateStatus(build.id, 'failed'));
            if (updateError) return console.error(updateError);
        } else if (code === 3) {
            console.error(`Build ${build.id} failed during image pushing.`);
            const [updateError] = await safe(builddb.updateStatus(build.id, 'failed'));
            if (updateError) return console.error(updateError);
        } else {
            console.error(`Build ${build.id} reported unknown error code ${code}`);
            const [updateError] = await safe(builddb.updateStatus(build.id, 'failed'));
            if (updateError) return console.error(updateError);
        }
    });
}

async function stop(buildId) {
    assert.strictEqual(typeof buildId, 'string');

    if (!gTasks[buildId]) return;

    gTasks[buildId].kill();
}

async function startLogStream(buildId, res) {
    const b = await builddb.get(buildId);
    if (!b) throw new Error('No such build');

    const buildLogFilePath = path.join('/tmp', `${buildId}.log`);
    fs.appendFileSync(buildLogFilePath, ''); // ensure file exists

    let lineNumber = 0;
    function sse(id, data) { return 'id: ' + id + '\ndata: ' + data + '\n\n'; }
    let statusCheckInterval = null;

    function done(error) {
        if (!statusCheckInterval) return;
        console.log(`startLogStream: finished with ${error?.message}`);
        clearInterval(statusCheckInterval);
        tail.unwatch();
        res.end();
        statusCheckInterval = null;
    }

    const tail = new Tail(buildLogFilePath, { fromBeginning: true });

    tail.on('line', function (data) {
        const json = safe.JSON.parse(data);
        if (json) {
            res.write(sse(lineNumber++, data));
        } else {
            res.write(sse(lineNumber++, JSON.stringify({ error: data }))); // send malformed line it as error
        }
    });

    tail.on('error', done);

    statusCheckInterval = setInterval(async function () {
        const [error, result] = await safe(get(buildId));
        if (error) return done(error);
        if (!result) return done(new Error('Build disappeared')); // build disappeared?

        if (result.status === 'success' || result.status === 'failed') {
            console.log(`startLogStream: build finished. ${result.status}`);
            done();
        }
    }, 1000);

    res.writeHead(200, {
        'Content-Type': 'text/event-stream',
        'Cache-Control': 'no-cache',
        'Connection': 'keep-alive',
        'X-Accel-Buffering': 'no', // disable nginx buffering
        'Access-Control-Allow-Origin': '*'
    });
    res.write('retry: 3000\n');
    res.on('close', () => {
        console.log('startLogStream: client closed');
        done();
    });
}

async function cleanupBuilds() {
    const allBuilds = await getAll();

    const buildsByRepo = {};
    Object.keys(allBuilds).forEach(function (id) {
        const build = allBuilds[id];
        const repo = build.dockerImageRepo;
        if (!buildsByRepo[repo]) buildsByRepo[repo] = [];
        buildsByRepo[repo].push(build);
    });

    const oldBuildIds = [];
    Object.keys(buildsByRepo).forEach(function (repo) {
        buildsByRepo[repo].sort((b1, b2) => b2.updatedAt - b1.updatedAt); // sort by time, latest first

        console.log(`\ncleanupBuilds repo: ${repo}`);
        for (const b of buildsByRepo[repo]) {
            console.log(`\t ${b.id} ${b.updatedAt}`);
        }

        if (buildsByRepo[repo][0].updatedAt > (Date.now() - 48*60*60*1000)) {
            console.log(`\t preserving the first build of ${repo}`);
            buildsByRepo[repo].shift(); // keep builds < 48 hours
        }

        for (const b of buildsByRepo[repo]) {
            console.log(`\t removing build ${b.id} with image ${b.dockerImageRepo}:${b.dockerImageTag}`);

            safe.child_process.execSync(`docker rmi ${b.dockerImageRepo}:${b.dockerImageTag}`);
            safe.fs.unlinkSync(b.sourceArchiveFilePath);
            safe.fs.unlinkSync(path.join('/tmp', `${b.id}.log`));

            oldBuildIds.push(b.id);
        }
    });

    console.log(`cleanupBuilds: removing the following builds: ${JSON.stringify(oldBuildIds)}`);

    await builddb.removeIds(oldBuildIds);
}
