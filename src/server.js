'use strict';

exports = module.exports = {
    start: start
};

var assert = require('assert'),
    bodyParser = require('body-parser'),
    builds = require('./builds.js'),
    crypto = require('crypto'),
    express = require('express'),
    fs = require('fs'),
    http = require('http'),
    HttpError = require('connect-lastmile').HttpError,
    HttpSuccess = require('connect-lastmile').HttpSuccess,
    lastMile = require('connect-lastmile'),
    multiparty = require('multiparty'),
    oidc = require('express-openid-connect'),
    safe = require('safetydance'),
    timeout = require('connect-timeout'),
    tokendb = require('./tokendb.js');

const PORT = 3000;
const APP_ORIGIN = process.env.CLOUDRON_APP_ORIGIN || `http://localhost:${PORT}`;
const TOKEN_LENGTH = 24;

function _mime(req) {
    var str = req.headers['content-type'] || '';
    return str.split(';')[0];
}

function multipart(req, res, next) {
    if (_mime(req) !== 'multipart/form-data') return res.status(400).send('Invalid content-type. Expecting multipart');

    var form = new multiparty.Form({
        uploadDir: '/tmp',
        keepExtensions: true,
        maxFieldsSize: 2 * 1024, // only field size, not files
        limit: '8mb', // file sizes
        autoFiles: true
    });

    // increase timeout of file uploads by default to 3 mins
    if (req.clearTimeout) req.clearTimeout(); // clear any previous installed timeout middleware

    timeout(3 * 60 * 1000 /* 3 minutes */)(req, res, function () {
        req.fields = {};
        req.files = {};

        form.parse(req, function (err/*, fields, files */) {
            if (err) return res.status(400).send('Error parsing request');
            next();
        });

        form.on('file', function (name, file) {
            req.files[name] = file;
        });

        form.on('field', function (name, value) {
            req.fields[name] = value; // otherwise fields.name is an array
        });
    });
}

async function issueToken(req, res, next) {
    const token = crypto.randomBytes(TOKEN_LENGTH).toString('hex');
    const username = req.username;

    const [delError] = await safe(tokendb.deleteByUsername(username));
    if (delError) return next(new HttpError(500, delError));

    const [addError] = await safe(tokendb.add(token, username));
    if (addError) return next(new HttpError(500, addError));

    next(new HttpSuccess(201, { token }));
}

async function profile(req, res, next) {
    if (req.query.accessToken) {
        const [error, result] = await safe(tokendb.get(req.query.accessToken));
        if (error) return next(new HttpError(500, error));

        return next(new HttpSuccess(200, { username: result.username, token: result.token }));
    }

    if (req.username) {
        const username = req.username;

        let [error, token] = await safe(tokendb.getByUsername.bind(null, username));
        if (error) return next(new HttpError(500, error));

        if (!token) {
            token = crypto.randomBytes(TOKEN_LENGTH).toString('hex');
            [error] = await safe(tokendb.add(token, username));
            if (error) return next(new HttpError(500, error));
        }

        return next(new HttpSuccess(200, { username, token }));
    }

    next(new HttpError(500, 'this route needs authenticate middleware'));
}

async function authenticate(req, res, next) {
    if (req.oidc.isAuthenticated()) {
        req.username = req.oidc.user.sub;
        return next();
    }

    if (!req.query.accessToken) return next(new HttpError(401, 'no session and no accessToken'));

    const [error, result] = await safe(tokendb.get(req.query.accessToken));
    if (error || !result) return next(new HttpError(401, 'Unauthorized'));

    req.username = result.username;

    next();
}

function oidcLogin(req, res) {
    res.oidc.login({
        returnTo: req.query.returnTo || '/',
        authorizationParams: {
            redirect_uri: `${APP_ORIGIN}/api/v1/login-callback`,
        },
    });
}

async function addBuild(req, res, next) {
    if (!req.files.sourceArchive) return next(new HttpError(400, 'sourceArchive is missing'));
    if (!req.fields.dockerImageRepo) return next(new HttpError(400, 'dockerImageRepo is missing'));
    if (!req.fields.dockerImageTag) return next(new HttpError(400, 'dockerImageTag is missing'));

    let buildArgs = [];
    if (req.fields.buildArgs) {
        try {
            buildArgs = JSON.parse(req.fields.buildArgs);
        } catch (e){
            console.error('Failed to parse buildArgs:', e);
            return next(new HttpError(400, 'buildArgs must be valid JSON'));
        }
    }

    if (typeof buildArgs !== 'object') return next(new HttpError(400, 'buildArgs must be a JSON object'));

    const build = {
        noPush: req.query.noPush === 'true',
        dockerImageRepo: req.fields.dockerImageRepo,
        dockerImageTag: req.fields.dockerImageTag,
        sourceArchiveFilePath: req.files.sourceArchive.path,
        noCache: req.query.noCache === 'true',
        dockerfileName: req.query.dockerfile || 'Dockerfile',
        buildArgs: buildArgs
    };

    const [error, id] = await safe(builds.add(build));
    if (error) return next(new HttpError(500, error));

    next(new HttpSuccess(201, { id }));
}

async function listBuilds(req, res, next) {
    const [error, result] = await safe(builds.getAll());
    if (error) return next(new HttpError(500, error));

    next(new HttpSuccess(200, { builds: result }));
}

async function getBuild(req, res, next) {
    assert.strictEqual(typeof req.params.buildId, 'string');

    const [error, result] = await safe(builds.get(req.params.buildId));
    if (error) return next(new HttpError(500, error));
    if (!result) return next(new HttpError(404, 'No such build'));

    next(new HttpSuccess(200, result));
}

async function pushBuild(req, res, next) {
    assert.strictEqual(typeof req.params.buildId, 'string');

    if (typeof req.body.dockerImageRepo !== 'string') return next(new HttpError(400, 'dockerImageRepo is required'));
    if (typeof req.body.dockerImageTag !== 'string') return next(new HttpError(400, 'dockerImageTag is required'));

    const [error] = await safe(builds.push(req.params.buildId, req.body.dockerImageRepo, req.body.dockerImageTag));
    if (error) return next(new HttpError(410, error)); // TODO: we need a real error object

    next(new HttpSuccess(201, {}));
}

async function stopBuild(req, res, next) {
    assert.strictEqual(typeof req.params.buildId, 'string');

    const [error] = await safe(builds.stop(req.params.buildId));
    if (error) return next(new HttpError(500, error));

    next(new HttpSuccess(201, {}));
}

async function getBuildLogStream(req, res, next) {
    assert.strictEqual(typeof req.params.buildId, 'string');

    if (req.headers.accept !== 'text/event-stream') return next(new HttpError(400, 'This API call requires EventStream'));

    req.clearTimeout();

    const [error] = await safe(builds.startLogStream(req.params.buildId, res));
    if (error) return next(new HttpError(500, error));
}

function healthcheck(req, res, next) {
    next(new HttpSuccess(200, {}));
}

function start(port, callback) {
    var app = express();
    var httpServer = http.createServer(app);

    var router = new express.Router();

    router.get  ('/healthcheck', healthcheck);

    // oidc login
    router.get  ('/api/v1/login', oidcLogin);

    router.get  ('/api/v1/profile', authenticate, profile);
    router.post ('/api/v1/token', authenticate, issueToken);

    router.get  ('/api/v1/builds', authenticate, listBuilds);
    router.post ('/api/v1/builds', authenticate, multipart, addBuild);
    router.get  ('/api/v1/builds/:buildId', authenticate, getBuild);
    router.post ('/api/v1/builds/:buildId/push', authenticate, pushBuild);
    router.delete('/api/v1/builds/:buildId', authenticate, stopBuild);
    router.get  ('/api/v1/builds/:buildId/logstream', authenticate, getBuildLogStream);

    // the timeout middleware will respond with a 503. the request itself cannot be 'aborted' and will continue
    // search for req.clearTimeout in route handlers to see places where this timeout is reset
    app.use(timeout(10000, { respond: true }));
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ extended: false, limit: '8mb' }));

    if (process.env.CLOUDRON_OIDC_ISSUER) {
        const SESSION_SECRET_FILE_PATH = '/app/data/.secret';
        if (!fs.existsSync(SESSION_SECRET_FILE_PATH)) {
            console.log('Generating new session secret...');
            fs.writeFileSync(SESSION_SECRET_FILE_PATH, crypto.randomBytes(20).toString('hex'), 'utf8');
        }

        // CLOUDRON_OIDC_PROVIDER_NAME is not supported
        app.use(oidc.auth({
            issuerBaseURL: process.env.CLOUDRON_OIDC_ISSUER,
            baseURL: APP_ORIGIN,
            clientID: process.env.CLOUDRON_OIDC_CLIENT_ID,
            clientSecret: process.env.CLOUDRON_OIDC_CLIENT_SECRET,
            secret: 'oidc-' + fs.readFileSync(SESSION_SECRET_FILE_PATH, 'utf8'),
            authorizationParams: {
                response_type: 'code',
                scope: 'openid profile email'
            },
            authRequired: false,
            routes: {
                callback: '/api/v1/login-callback',
                login: false,
                logout: '/api/v1/logout'
            },
            session: {
                rolling: true,
                rollingDuration: 24 * 60 * 60 * 4 // max 4 days idling
            },
        }));
    } else {
        // mock oidc
        app.use(require('express-session')({ secret: 'local dev session', resave: false, saveUninitialized: false }));
        app.use((req, res, next) => {
            res.oidc = {
                login(options) {
                    res.writeHead(200, { 'Content-Type': 'text/html' });
                    res.write(require('fs').readFileSync(__dirname + '/oidc_develop_user_select.html', 'utf8').replaceAll('REDIRECT_URI', options.authorizationParams.redirect_uri));
                    res.end();
                }
            };

            req.oidc = {
                user: {},
                isAuthenticated() {
                    return !!req.session.username;
                }
            };

            if (req.session.username) {
                req.oidc.user = {
                    sub: req.session.username,
                    family_name: 'Cloudron',
                    given_name: req.session.username.toUpperCase(),
                    locale: 'en-US',
                    name: req.session.username.toUpperCase() + ' Cloudron',
                    preferred_username: req.session.username,
                    email: req.session.username + '@cloudron.local',
                    email_verified: true
                };
            }

            next();
        });

        app.use('/api/v1/login-callback', (req, res) => {
            req.session.username = req.query.username;
            res.redirect(APP_ORIGIN);
        });

        app.use('/api/v1/logout', (req, res) => {
            req.session.username = null;
            res.status(200).send({});
        });
    }

    app.use(router);
    app.use(express.static('./frontend/dist'));
    app.use(lastMile());

    httpServer.setTimeout(0); // disable server socket "idle" timeout. we use timeout middleware

    httpServer.listen(port, '0.0.0.0', callback);
}
