'use strict';

var assert = require('assert'),
    path = require('path'),
    fs = require('fs');

exports = module.exports = {
    add,
    getByUsername,
    get,
    deleteByUsername
};

// TODO replace with real db
var DB_FILE_PATH = process.env.CLOUDRON ? '/app/data/tokendb.json' : path.resolve('tokendb.json');

async function getDatabase() {
    if (!fs.existsSync(DB_FILE_PATH)) return {};

    return JSON.parse(fs.readFileSync(DB_FILE_PATH, 'utf8').toString());
}

async function setDatabase(data) {
    fs.writeFileSync(DB_FILE_PATH, JSON.stringify(data, null, 4), 'utf8');
}

async function get(token) {
    assert.strictEqual(typeof token, 'string');

    const db = await getDatabase();
    if (!db[token]) return null;

    return db[token];
}

async function deleteByUsername(username) {
    assert.strictEqual(typeof username, 'string');

    const db = await getDatabase();

    for (const token of Object.keys(db)) {
        if (db[token].username === username) {
            delete db[token];
        }
    }

    await setDatabase(db);
}

async function getByUsername(username) {
    assert.strictEqual(typeof username, 'string');

    const db = await getDatabase();

    for (const token of Object.keys(db)) {
        if (db[token].username === username) return token;
    }

    return null;
}

async function add(token, username) {
    assert.strictEqual(typeof token, 'string');
    assert.strictEqual(typeof username, 'string');

    const db = await getDatabase();

    db[token] = {
        token,
        username
    };

    await setDatabase(db);
}

