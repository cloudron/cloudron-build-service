'use strict';

var builddb = require('./builddb.js'),
    builds = require('./builds.js');

exports = module.exports = {
    initialize
};

async function advance() {
    try {
        const result = await builddb.getPending();
        if (!result.length) return;

        result.forEach(function (build) {
            builds.start(build);
        });
    } catch (error) {
        console.error('Failed to process build tasks.', error);
    }
}

function initialize() {
    setInterval(advance, 2000);
}
