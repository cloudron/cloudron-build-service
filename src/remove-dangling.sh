#!/bin/bash

set -eu

for image_id in $(docker images -q --filter "dangling=true"); do
    created_date=$(docker inspect -f '{{.Created}}' $image_id)
    created_epoch=$(date -d"${created_date}" +%s)
    now=$(date +%s)
    if [[ $(($now - $created_epoch)) -gt 3600 ]]; then # older than 1 hour
        echo "Removing dangling image $image_id since it is too old"
        docker rmi ${image_id} || true
    fi
done

