'use strict';

var assert = require('assert'),
    path = require('path'),
    fs = require('fs');

exports = module.exports = {
    add,
    get,
    getAll,
    update,
    updateStatus,
    getPending,
    removeIds
};

// TODO replace with real db
var DB_FILE_PATH = process.env.CLOUDRON ? '/app/data/builddb.json' : path.resolve('builddb.json');

async function getDatabase() {
    if (!fs.existsSync(DB_FILE_PATH)) return {};

    let db = {};
    try {
        db = JSON.parse(fs.readFileSync(DB_FILE_PATH, 'utf8').toString());
    } catch (error) {
        console.error('Unable to parse builddb.json. Starting with a fresh one...', error);
    }

    return db;
}

async function setDatabase(data) {
    fs.writeFileSync(DB_FILE_PATH, JSON.stringify(data, null, 4), 'utf8');
}

async function get(id) {
    assert.strictEqual(typeof id, 'string');

    const result = await getDatabase();
    if (!result[id]) return null;

    return result[id];
}

async function getAll() {
    return await getDatabase();
}

async function add(build) {
    assert.strictEqual(typeof build, 'object');
    assert.strictEqual(typeof build.id, 'string');

    const result = await getDatabase();
    result[build.id] = build;
    result[build.id].status = 'pending';
    result[build.id].createdAt = result[build.id].updatedAt = Date.now();

    await setDatabase(result);
}

async function getPending() {
    const result = await getDatabase();
    const pending = Object.keys(result).filter(function (id) { return result[id].status.startsWith('pending'); }).map(function (id) { return result[id]; });

    return pending;
}

async function removeIds(ids) {
    const result = await getDatabase();

    ids.forEach(id => delete result[id]);

    await setDatabase(result);
}

// status is 'success', 'running, 'failed', 'pending_push', 'pending'
async function updateStatus(id, status) {
    assert.strictEqual(typeof id, 'string');
    assert.strictEqual(typeof status, 'string');

    const result = await getDatabase();
    if (!result[id]) throw new Error(`build ${id} not found`);

    result[id].status = status;
    result[id].createdAt = result[id].updatedAt = Date.now();

    await setDatabase(result);
}

async function update(id, data) {
    assert.strictEqual(typeof id, 'string');
    assert.strictEqual(typeof data, 'object');

    const result = await getDatabase();
    if (!result[id]) throw new Error(`build ${id} not found`);

    result[id] = Object.assign(result[id], data);
    result[id].updatedAt = Date.now();

    await setDatabase(result);
}
